# ExamGenerator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5.

## API key
Before you can build the application you need to supply your GPT-3 API key in the file [src/module/shared/services/base-open-api-json.service.ts](./src/module/shared/services/base-open-api-json.service.ts).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Docker Compose

Run `docker-compose up -d` for a dev server in Docker. Navigate to `http://localhost:8080/`.
