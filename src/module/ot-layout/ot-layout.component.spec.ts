import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtLayoutComponent } from './ot-layout.component';

describe('OtLayoutComponent', () => {
  let component: OtLayoutComponent;
  let fixture: ComponentFixture<OtLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
