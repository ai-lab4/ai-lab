import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ot-layout',
  templateUrl: './ot-layout.component.html',
  styleUrls: ['./ot-layout.component.sass']
})
export class OtLayoutComponent implements OnInit {

  constructor() { }

  @Input() question = ['Question: What does the fox say?', 'It screams very load'];

  ngOnInit(): void {
  }

}
