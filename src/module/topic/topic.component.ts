import { Component, OnInit } from '@angular/core';
import {OpenApiService} from "../shared/services/openai.service";
import {RootObject} from "../../app/OpenAiResponse";
import {map, Observable, startWith} from "rxjs";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.sass']
})
export class TopicComponent implements OnInit {

  topic = new FormControl();
  questionType: string = "";
  questionTypes: string[] = ['Multiple Choice Question', 'Text Question'];
  loading: boolean = false;
  prompt: string = ""
  question: string[] = [];
  difficulty: number = 0;
  options: string[] = ['Natural Language Processing', 'Moon Landing', 'First World War'];
  filteredOptions: Observable<string[]>;
  multiple: boolean = false

  constructor(private openAiService: OpenApiService) {
    this.filteredOptions = this.topic.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }

  getDifficulty() {
    switch (this.difficulty) {
      case 0:
        return 'easy';
      case 1:
        return 'medium';
      case 2:
        return 'difficult';
      default:
        return 'God mode'
    }
  }

  getScheme() {
    switch (this.questionType) {
      case this.questionTypes[0]:
        return "Question: QUESTION\n" +
          "A) OPTION A\n" +
          "B) OPTION B\n" +
          "C) OPTION C\n" +
          "Answer: CORRECT ANSWER";
      case this.questionTypes[1]:
        return "Question: QUESTION\n" +
          "Answer: CORRECT ANSWER";
      default:
        return "";
    }
  }

  getQuestion(): void {
    this.loading = true;
    const prompt = `Write a ${this.questionType} ${this.questionType === 'Multiple Choice Question' ? "containing 3 options" : ""} about the topic of ${this.topic.value} ${this.multiple ? 'where multiple answers are correct' : ''} and give the correct answer according to the following scheme \n${this.getScheme()}`;
    this.prompt = prompt;
    console.log(prompt)
    this.openAiService.getQuestion(prompt).subscribe(
      {
        next: (res: RootObject) => {
          this.loading = false;
          if (res.choices.length > 0) {
            this.processQuestion(res.choices[0].text)
          }
          console.log(res.choices);
        },
        error: (error: Error) => {
          this.loading = false;
          console.log(error);
        }
      }
    );
  }

  processQuestion(questionString: string) {
    var parts = questionString.split("\n")
    parts = parts.filter(n => n !== '')
    this.question = parts;
  }

  valid() {
    return this.loading || this.topic.value === '' || this.questionType === '' || !this.topic.value;
  }

  ngOnInit(): void {
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
}

