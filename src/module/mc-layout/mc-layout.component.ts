import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-mc-layout',
  templateUrl: './mc-layout.component.html',
  styleUrls: ['./mc-layout.component.sass']
})
export class McLayoutComponent implements OnInit {
  panelOpenState: boolean = false;

  @Input() question = ['Question: What does the fox say?', 'A) WREEE', 'B) Miau', 'C) Wuff', 'A)'];

  constructor() { }

  ngOnInit(): void {
  }

}
