import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';

export interface HttpOptions {
  headers: HttpHeaders;
  params: HttpParams;
  body: {};
}

const key = <YOUR APIKEY HERE>;


@Injectable()
export class BaseOpenApiJsonService {
  protected options: HttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + key
    }),
    params: new HttpParams(),
    body: {}
  };

  constructor(protected http: HttpClient) {
  }
  protected handleError(error: HttpErrorResponse): Observable<never> {
    return throwError(error || 'Server error');
  }
}
