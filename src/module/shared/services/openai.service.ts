import {Injectable} from '@angular/core';
import {BaseOpenApiJsonService} from "./base-open-api-json.service";
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable} from "rxjs";
import {RootObject} from "../../../app/OpenAiResponse";

@Injectable({
  providedIn: 'root'
})

export class OpenApiService extends BaseOpenApiJsonService {
  shortMessageMaxLen: number = 100;
  shortPromptParameterSet = {
    "temperature": 0.7,
    "frequency_penalty": 0.3,
    "presence_penalty": 0.4
  };
  longPromptParameterSet = {
    "temperature": 0.85,
    "frequency_penalty": 0.2,
    "presence_penalty": 0.2
  }

  constructor(public override http: HttpClient) {
    super(http);
  }

  getQuestion(prompt: string): Observable<RootObject> {
    const paramSet = prompt.length > this.shortMessageMaxLen ? this.longPromptParameterSet : this.shortPromptParameterSet;
    const url = "https://api.openai.com/v1/engines/text-davinci-002/completions";
    const body = {
      "prompt": prompt,
      "temperature": paramSet["temperature"],
      "max_tokens": 256,
      "top_p": 1.0,
      "frequency_penalty": paramSet["frequency_penalty"],
      "presence_penalty": paramSet["presence_penalty"]
    }

    return this.http.post(url, body, this.options).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(this.handleError)
    );
  }
}
