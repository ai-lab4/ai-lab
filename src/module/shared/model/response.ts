export interface ServerResponse {
  data: any;
  error: string;
}
