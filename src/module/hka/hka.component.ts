import { Component, OnInit } from '@angular/core';
import {OpenApiService} from "../shared/services/openai.service";
import {RootObject} from "../../app/OpenAiResponse";
import {map, Observable, startWith} from "rxjs";
import {FormControl} from "@angular/forms";
interface Lecture {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-hka',
  templateUrl: './hka.component.html',
  styleUrls: ['./hka.sass']
})
export class HkaComponent implements OnInit {

  topic: string = '';
  questionType: string = "";
  questionTypes: string[] = ['Multiple Choice Question', 'Text Question'];
  loading: boolean = false;
  prompt: string = ""
  question: string[] = [];
  difficulty: number = 0;
  lectures: Lecture[] = [
    {value: 'If the data link layer waits longer\nthan the sender’s timeout period, the frame will be retransmitted, defeating the\nwhole purpose of having acknowledgements. If the data link layer were an oracle\nand could foretell the future, it would know when the next network layer packet\nwas going to come in and could decide either to wait for it or send a separate ac-\nknowledgement immediately, depending on how long the projected wait was\ngoing to be. Of course, the data link layer cannot foretell the future, so it must\nresort to some ad hoc scheme, such as waiting a fixed number of milliseconds. If\na new packet arrives quickly, the acknowledgement is piggybacked onto it.\nOtherwise, if no new packet has arrived by the end of this time period, the data\nlink layer just sends a separate acknowledgement frame.\nThe next three protocols are bidirectional protocols that belong to a class cal-\nled sliding window protocols. The three differ among themselves in terms of ef-\nficiency, complexity, and buffer requirements, as discussed later. In these, as in\nall sliding window protocols, each outbound frame contains a sequence number,\nranging from 0 up to some maximum. The maximum is usually 2n − 1 so the se-\nquence number fits exactly in an n-bit field. The stop-and-wait sliding window\nprotocol uses n = 1, restricting the sequence numbers to 0 and 1, but more sophis-\nticated versions can use an arbitrary n.\nThe essence of all sliding window protocols is that at any instant of time, the\nsender maintains a set of sequence numbers corresponding to frames it is permit-\nted to send. These frames are said to fall within the sending window. Similarly,\nthe receiver also maintains a receiving window corresponding to the set of frames\nit is permitted to accept. The sender’s window and the receiver’s window need\nnot have the same lower and upper limits or even have the same size. In some\nprotocols they are fixed in size, but in others they can grow or shrink over the\ncourse of time as frames are sent and received.\nAlthough these protocols give the data link layer more freedom about the\norder in which it may send and receive frames, we have definitely not dropped the\nrequirement that the protocol must deliver packets to the destination network layer\nin the same order they were passed to the data link layer on the sending machine.\nNor have we changed the requirement that the physical communication channel is\n‘‘wire-like,’’ that is, it must deliver all frames in the order sent.\nThe sequence numbers within the sender’s window represent frames that have\nbeen sent or can be sent but are as yet not acknowledged. Whenever a new packet\narrives from the network layer, it is given the next highest sequence number, and\nthe upper edge of the window is advanced by one. When an acknowledgement\ncomes in, the lower edge is advanced by one. In this way the window continu-\nously maintains a list of unacknowledged frames. Figure 3-15 shows an example.\nSince frames currently within the sender’s window may ultimately be lost or\ndamaged in transit, the sender must keep all of these frames in its memory for\npossible retransmission. Thus, if the maximum window size is n, the sender needs\nn buffers to hold the unacknowledged frames.', viewValue: 'Mobile Systeme Prof. Waldhorst (Link Layer)'},
    {value: '\n8.1 Data, Models, and Learning 257\n8.1.4 Learning is Finding Parameters\nThe goal of learning is to find a model and its corresponding parame-\nters such that the resulting predictor will perform well on unseen data.\nThere are conceptually three distinct algorithmic phases when discussing\nmachine learning algorithms:\n1. Prediction or inference\n2. Training or parameter estimation\n3. Hyperparameter tuning or model selection\nThe prediction phase is when we use a trained predictor on previously un-\nseen test data. In other words, the parameters and model choice is already\nfixed and the predictor is applied to new vectors representing new input\ndata points. As outlined in Chapter 1 and the previous subsection, we will\nconsider two schools of machine learning in this book, corresponding to\nwhether the predictor is a function or a probabilistic model. When we\nhave a probabilistic model (discussed further in Section 8.4) the predic-\ntion phase is called inference.\nRemark. Unfortunately, there is no agreed upon naming for the different\nalgorithmic phases. The word “inference” is sometimes also used to mean\nparameter estimation of a probabilistic model, and less often may be also\nused to mean prediction for non-probabilistic models. ♦\nThe training or parameter estimation phase is when we adjust our pre-\ndictive model based on training data. We would like to find good predic-\ntors given training data, and there are two main strategies for doing so:\nfinding the best predictor based on some measure of quality (sometimes\ncalled finding a point estimate), or using Bayesian inference. Finding a\npoint estimate can be applied to both types of predictors, but Bayesian\ninference requires probabilistic models.\nFor the non-probabilistic model, we follow the principle of empirical risk empirical risk\nminimizationminimization, which we describe in Section 8.2. Empirical risk minimiza-\ntion directly provides an optimization problem for finding good parame-\nters. With a statistical model, the principle of maximum likelihood is used maximum likelihood\nto find a good set of parameters (Section 8.3). We can additionally model\nthe uncertainty of parameters using a probabilistic model, which we will\nlook at in more detail in Section 8.4.\nWe use numerical methods to find good parameters that “fit” the data,\nand most training methods can be thought of as hill-climbing approaches\nto find the maximum of an objective, for example the maximum of a likeli-\nhood. To apply hill-climbing approaches we use the gradients described in The convention in\noptimization is to\nminimize objectives.\nHence, there is often\nan extra minus sign\nin machine learning\nobjectives.\nChapter 5 and implement numerical optimization approaches from Chap-\nter 7.\nAs mentioned in Chapter 1, we are interested in learning a model based\non data such that it performs well on future data. It is not enough for\nc©2021 M. P. Deisenroth, A. A. Faisal, C. S. Ong. Published by Cambridge University Press (2020).\n258 When Models Meet Data\nthe model to only fit the training data well, the predictor needs to per-\nform well on unseen data. We simulate the behavior of our predictor on\nfuture unseen data using cross-validation (Section 8.2.4). As we will seecross-validation\nin this chapter, to achieve the goal of performing well on unseen data,\nwe will need to balance between fitting well on training data and finding\n“simple” explanations of the phenomenon. This trade-off is achieved us-\ning regularization (Section 8.2.3) or by adding a prior (Section 8.3.2). In\nphilosophy, this is considered to be neither induction nor deduction, but\nis called abduction. According to the Stanford Encyclopedia of Philosophy,abduction\nabduction is the process of inference to the best explanation (Douven,\n2017).A good movie title is\n“AI abduction”. We often need to make high-level modeling decisions about the struc-\nture of the predictor, such as the number of components to use or the\nclass of probability distributions to consider. The choice of the number of\ncomponents is an example of a hyperparameter, and this choice can af-hyperparameter\nfect the performance of the model significantly. The problem of choosing\namong different models is called model selection, which we describe inmodel selection\nSection 8.6. For non-probabilistic models, model selection is often done\nusing nested cross-validation, which is described in Section 8.6.1. We alsonested\ncross-validation use model selection to choose hyperparameters of our model.\nRemark. The distinction between parameters and hyperparameters is some-\nwhat arbitrary, and is mostly driven by the distinction between what can\nbe numerically optimized versus what needs to use search techniques.\nAnother way to consider the distinction is to consider parameters as the\nexplicit parameters of a probabilistic model, and to consider hyperparam-\neters (higher-level parameters) as parameters that control the distribution\nof these explicit parameters. ♦\nIn the following sections, we will look at three flavors of machine learn-\ning: empirical risk minimization (Section 8.2), the principle of maximum\nlikelihood (Section 8.3), and probabilistic modeling\n', viewValue: 'Machine Learning Prof. Laubenheimer (Lernen)'},
    {value: '\nA transformer is a deep learning model that adopts the mechanism of self-attention, differentially weighting the significance of each part of the input data. It is used primarily in the fields of natural language processing (NLP)[1] and computer vision (CV).[2]\n\nLike recurrent neural networks (RNNs), transformers are designed to process sequential input data, such as natural language, with applications towards tasks such as translation and text summarization. However, unlike RNNs, transformers process the entire input all at once. The attention mechanism provides context for any position in the input sequence. For example, if the input data is a natural language sentence, the transformer does not have to process one word at a time. This allows for more parallelization than RNNs and therefore reduces training times.[1]\n\nTransformers were introduced in 2017 by a team at Google Brain[1] and are increasingly the model of choice for NLP problems,[3] replacing RNN models such as long short-term memory (LSTM). The additional training parallelization allows training on larger datasets than was once possible. This led to the development of pretrained systems such as BERT (Bidirectional Encoder Representations from Transformers) and GPT (Generative Pre-trained Transformer), which were trained with large language datasets, such as the Wikipedia Corpus and Common Crawl, and can be fine-tuned for specific tasks.[4][5] \n', viewValue: 'KI Prof. Baier (Transformer)'},
    {value: "\nProject management is the process of leading the work of a team to achieve all project goals within the given constraints.[1] This information is usually described in project documentation, created at the beginning of the development process. The primary constraints are scope, time, and budget.[2] The secondary challenge is to optimize the allocation of necessary inputs and apply them to meet pre-defined objectives.\n\nThe objective of project management is to produce a complete project which complies with the client's objectives. In many cases, the objective of project management is also to shape or reform the client's brief to feasibly address the client's objectives. Once the client's objectives are clearly established they should influence all decisions made by other people involved in the project – for example, project managers, designers, contractors, and sub-contractors. Ill-defined or too tightly prescribed project management objectives are detrimental to decision making.\n\nA project is a temporary and unique endeavor designed to produce a product, service, or result with a defined beginning and end (usually time-constrained, and often constrained by funding or staffing) undertaken to meet unique goals and objectives, typically to bring about beneficial change or added value.[3][4] The temporary nature of projects stands in contrast with business as usual (or operations),[5] which are repetitive, permanent, or semi-permanent functional activities to produce products or services. In practice, the management of such distinct production approaches requires the development of distinct technical skills and management strategies.[6] \n", viewValue: 'IT-Projektmanagement Prof. Haneke (Definition)'},
  ];
  multiple: boolean = false;

  constructor(private openAiService: OpenApiService) {

  }

  getDifficulty() {
    switch (this.difficulty) {
      case 0:
        return 'easy';
      case 1:
        return 'medium';
      case 2:
        return 'difficult';
      default:
        return 'God mode'
    }
  }

  getScheme() {
    switch (this.questionType) {
      case this.questionTypes[0]:
        return "Question: QUESTION\n" +
          "A) OPTION A\n" +
          "B) OPTION B\n" +
          "C) OPTION C\n" +
          "Answer: CORRECT ANSWER";
      case this.questionTypes[1]:
        return "Question: QUESTION\n" +
          "Answer: CORRECT ANSWER";
      default:
        return "";
    }
  }

  getQuestion(): void {
    console.log(this.topic)
    this.loading = true;
    const prompt = `${this.topic} \n\n Write a ${this.questionType} ${this.questionType === 'Multiple Choice Question' ? "containing 3 options" : ""} from the text above ${this.multiple ? 'where multiple answers are correct' : ''} and give the correct answer according to the following scheme \n${this.getScheme()}`;
    this.prompt = prompt;
    console.log(prompt)
    this.openAiService.getQuestion(prompt).subscribe(
      {
        next: (res: RootObject) => {
          this.loading = false;
          if (res.choices.length > 0) {
            this.processQuestion(res.choices[0].text)
          }
          console.log(res.choices);
        },
        error: (error: Error) => {
          this.loading = false;
          console.log(error);
        }
      }
    );
  }

  processQuestion(questionString: string) {
    var parts = questionString.split("\n")
    parts = parts.filter(n => n !== '')
    this.question = parts;
  }

  valid() {
    return this.loading || this.topic === '' || this.questionType === '';
  }

  ngOnInit(): void {
  }

}

