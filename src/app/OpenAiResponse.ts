export interface Choice {
  text: string;
  index: number;
  logprobs?: any;
  finish_reason: string;
}

export interface RootObject {
  id: string;
  object: string;
  created: number;
  model: string;
  choices: Choice[];
}
