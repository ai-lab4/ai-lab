FROM node:18.1.0 as builder
RUN mkdir /build

COPY /package.json ./build
COPY /package-lock.json ./build
WORKDIR /build
RUN npm install

COPY . .
RUN npm install -g npm@8.10.0
RUN npm run build

FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /build/dist /usr/share/nginx/html

WORKDIR /usr/share/nginx/htm/
